/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema1;

/**
 *
 * @author Juarez
 */
public class Forma implements Color{
    private Color color;

    public Forma(Color color) {
        this.color=color;
    }

    @Override
    public String getColor() {
        return color.getColor();
    }
    
}