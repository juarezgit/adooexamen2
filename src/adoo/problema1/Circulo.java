/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema1;

/**
 *
 * @author Juarez
 */
public class Circulo extends Forma{
    private double perimetro;
    private double area;
    private double radio;

    public Circulo(double perimetro, double area, double radio, Color color) {
        super(color);
        this.perimetro = perimetro;
        this.area = area;
        this.radio = radio;
    }
   
    public double getPerimetro() {
        return perimetro;
    }

    public double getArea() {
        return area;
    }

    public double getRadio() {
        return radio;
    }

    @Override
    public String getColor() {
        return super.getColor(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}