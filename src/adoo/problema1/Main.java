/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema1;

/**
 *
 * @author Juarez
 */
public class Main {
    
    
    
    public static void main(String []args){
        Forma circuloRojo = new Circulo(10, 50, 40,new Rojo());
        Forma circuloAzul = new Circulo(10, 50, 40,new Azul());
               
        System.out.println("El circulo es color " +circuloRojo.getColor());
        System.out.println("El circulo es color " +circuloAzul.getColor());
        
        Forma cuadradoRojo = new Cuadrado(5, new Rojo());
        Forma cuadradoAzul = new Cuadrado(5, new Azul());
        System.out.println("\nEl cuadrado es color " +cuadradoRojo.getColor());
        System.out.println("El cuadrado es color " +cuadradoAzul.getColor());

        
    }
   
}
