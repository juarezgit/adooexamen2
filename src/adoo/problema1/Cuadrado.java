/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema1;

/**
 *
 * @author Juarez
 */
public class Cuadrado extends Forma{
    private double lado;

    public Cuadrado(double lado, Color color) {
        super(color);
        this.lado = lado;
    }
    
    public double getPerimetro(){
        return lado*4;
    }
    
    public double getArea(){
        return lado*lado;
    }
    
    @Override
    public String getColor() {
        return super.getColor(); //To change body of generated methods, choose Tools | Templates.
    }

}