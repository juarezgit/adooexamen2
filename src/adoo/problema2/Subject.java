/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema2;

/**
 *
 * @author Juarez
 */
public interface Subject {
    
    public void attach(IManEmail observador);
    public void dettach(IManEmail observador);
    public void notifyObservers();
    
}
