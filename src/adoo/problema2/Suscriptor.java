/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema2;

/**
 *
 * @author Juarez
 */
public class Suscriptor {
    private String nombre;
    private String email;
    private String status;

    public Suscriptor(String nombre, String email, String status) {
        this.nombre = nombre;
        this.email = email;
        this.status = status;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public String getStatus() {
        return status;
    }
 
}