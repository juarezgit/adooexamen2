/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema2;

import java.util.ArrayList;

/**
 *
 * @author Juarez
 */
public class NotificarEmail implements Subject{

    private static ArrayList<IManEmail> observadores = new ArrayList<>();
       
    @Override
    public void attach(IManEmail observador) {
        observadores.add(observador);
    }

    @Override
    public void dettach(IManEmail observador) {
        observadores.remove(observador);
    }

    @Override
    public void notifyObservers() {
        for(int i=0; i<observadores.size();i++){
            observadores.get(i).actualizar();
        }
    }
    
    
}
