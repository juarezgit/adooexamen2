/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema3;

import java.util.ArrayList;

/**
 *
 * @author Juarez
 */
public interface IPizza {
    
    public void imprimirIngredientes();
    public String getTamano();
    public void setIngredientes(ArrayList<String> ingredientes);
    public void setTamano(String tam);
        
}
