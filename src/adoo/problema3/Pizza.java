/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema3;

import java.util.ArrayList;

/**
 *
 * @author Juarez
 */
public class Pizza implements IPizza{
    private ArrayList<String> ingredientes;
    private String tam;

    public Pizza(ArrayList<String> ingredientes, String tam) {
        this.ingredientes = ingredientes;
        this.tam = tam;
    }

    @Override
    public void imprimirIngredientes() {
        System.out.println("ingredientes");
        for (String ingrediente : ingredientes) {
            System.out.println(ingrediente);
        }
    }    

    @Override
    public String getTamano() {
        return tam;
    }

    @Override
    public void setIngredientes(ArrayList<String> ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public void setTamano(String tam) {
        this.tam=tam;
    }
    
}
