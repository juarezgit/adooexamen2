/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema3;

import java.util.ArrayList;

/**
 *
 * @author Juarez
 */
public class Recetario {
    private IPizza pizza;

    public Recetario(IPizza pizza) {
        this.pizza = pizza;
    }
    
    public void imprimirIngredientes(){
        pizza.imprimirIngredientes();
    }
    
    public void setIngredientes(ArrayList<String> ingredientes){
        pizza.setIngredientes(ingredientes);
    }
    
}
