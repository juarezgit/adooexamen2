/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adoo.problema3;

import java.util.ArrayList;

/**
 *
 * @author Juarez
 */
public class Main {
    
    public static void main(String []args){
        ArrayList<String> ingredientes = new ArrayList<>();
        ingredientes.add("harina");
        ingredientes.add("salsa");
        ingredientes.add("peperoni");
        ingredientes.add("jamon");
        ingredientes.add("queso");

        Pizza pizza = new Pizza(ingredientes, "Grande");
        pizza.imprimirIngredientes();
        
        //Adapter
        Recetario recetario = new Recetario(pizza);
        ArrayList<String> ingredientesPastel = new ArrayList<>();
        ingredientesPastel.add("harina");
        ingredientesPastel.add("huevo");
        ingredientesPastel.add("azucar");
        ingredientesPastel.add("agua");
        
        System.out.println("\nPastel");
        recetario.setIngredientes(ingredientesPastel);
        recetario.imprimirIngredientes();
        
    }
}